Hippodamia observe the state of all registered microservices (aka watch dog).

![Pelops Overview](img/Microservice Overview.png)

```Hippodamia``` is part of the collection of mqtt based microservices [pelops](https://gitlab.com/pelops). An overview
on the microservice architecture and examples can be found at (http://gitlab.com/pelops/pelops).

# for Developers

## States of a Microservice

![AState Diagram](img/microservice_states.png)

  * _initialize phase_ - an onboarding request has been received by the system. if the source has been identified as a 
                         known microservice, the existing state will be used, a new one will be initialized. 
  * _onboarding_ - a microserivce has been identified/initialized and the system waits for it to react to the onboarding
                   request response.
  * _active_ - the observed microservice has been successfully onboarded and sends state updates regularly.
  * _inactive_ - the microservice has not sent any update for a predefined period.
  * _terminated_ - the microservice has either send a good-by-message or been inactive for to long.
  * _end state_ - the system has terminated observation of this microservice.

## States of the Corresponding Agent
![AgentState_Diagram](../hippodamia-agent/img/agent_states.png)
  * _uninitialized_ - agent has just started or a restart has been forced. on entry of this state a new uuid will be 
  generated. 
  * _initizalized_ - agent is ready to start onboarding procedure.
  * _onboading_ - agent has sent an onboarding request and is waiting for an reply
  * _active_ - agent is constantly sending ping, runtime, and config messages
  * _terminating_ - agent sends an termination message to the service before shutdown 

## Topics
see [AsyncAPI](docs/index.html).

### Incoming
  * _contact_ - for onboarding/offboarding requests
  * _state.ping_ - listens for new ping messages
  * _state.runtime_ - listens for new runtime messages
  * _state.config_ - listens for new config messages
  * _state.end_ - listens for termination messages
### Outgoing
  * _uuid_ - individual topic opened by each microservice for the onboarding process
  * _command.ping_ - request ping messages
  * _command.runtime_ - request runtime messages
  * _command.config_ - request config messages
  * _command.onboarding_ - request re-onboarding

## Messages
see [AsyncAPI](docs/index.html).

### Incoming
  * _onboarding request_ - onboarding request from a microservice
  * _ping_ - minimum "sign of life" of a microservice
  * _runtime info_ - ping plus additional runtime information
  * _config state_ - ping plus service configuration
  * _termination info_ - end service signal. sent either upon stop of service or as last will via mqtt server. 
  
### Outgoing
  * _onboarding response_ - onboarding response sent by hippodamia to the microservice via the provide onboarding topic from the microservice 
  * _re-onboard request_ - request plus optionally gid
  * _request ping_ - request plus optionally gid
  * _request runtime info_ - request plus optionally gid
  * _request full state_ - request plus optionally gid
  
### Onboarding Sequence
![Sequence](img/onboarding.png)  

First, the microservice subscribes to a unique topic. This topic is sent together with additional information to
identify the microservice (especially to look if this microservices has been onboarded previously) to hippodamias
onboarding channel. Hippodamia answers with the onboarding response message which primarily contains the gid - the 
identifier of this particular microservice instance that ideally should be the same even after the n-th onboarding 
cycle. The repsonse message is published to the unique topic from the microservice. Once the microservice has received
its gid, the uniqued topic can be unsubscribed and the reqular topics will be used for further communication. 

### Ping Sequence
![Ping](img/ping.png)

Ping messages are normally sent by the microservice at arbitrary times. Usually a sent interval
of e.g. 60 seconds is implemented. The other possibility is that hippodamia requests a ping 
message from all onboarded microservices. The same sequence applies to runtime, config and onboarding.

## Database

Hippodamia stores all info immediately into the database. 
  * Microservice state
  * Received messages
  