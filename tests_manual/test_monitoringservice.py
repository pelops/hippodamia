import unittest
import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from hippodamia.monitoringservice import Monitoringservice


Monitoringservice.standalone(args=["-c", "../tests_unit/config.yaml"])
