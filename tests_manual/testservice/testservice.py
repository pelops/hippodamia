from pelops.abstractmicroservice import AbstractMicroservice
from pelops.mythreading import LoggerThread
from threading import Event
import random
from pelops.logging.mylogger import get_child


class TestService(AbstractMicroservice):
    _version = 0.1
    _monitoring_agent = None
    _thread = None
    _stop_thread = None

    def __init__(self, config, mqtt_client=None, logger=None, stdout_log_level=None, no_gui=None):
        """
        Constructor - creates the services and the tasks

        :param config: config yaml structure
        :param mqtt_client: mqtt client instance
        :param logger: logger instance
        """
        AbstractMicroservice.__init__(self, config, "testservice", mqtt_client, logger, self.__class__.__name__,
                                      stdout_log_level=stdout_log_level, no_gui=no_gui)
        self._stop_thread = Event()
        self._thread = LoggerThread(target=self._run, logger=self._logger, name="TestService")
        self._logger = get_child(self._logger, "a")
        self._logger = get_child(self._logger, "b")

    def _run(self):
        counter = [0, 0, 0, 0, 0]
        while not self._stop_thread.is_set():
            if not self._stop_thread.wait(random.random()*3):
                i = 9
                while i > 4:
                    i = int(random.triangular(0, 9))
                if i == 4:
                    counter[i] = counter[i] + 1
                    self._logger.debug("debug - ... Testservice entry {}".format(counter[i]))
                elif i == 3:
                    counter[i] = counter[i] + 1
                    self._logger.info("info - ... Testservice entry {}".format(counter[i]))
                elif i == 2:
                    counter[i] = counter[i] + 1
                    self._logger.warning("warning - ... Testservice entry {}".format(counter[i]))
                elif i == 1:
                    counter[i] = counter[i] + 1
                    self._logger.error("error - ... Testservice entry {}".format(counter[i]))
                elif i == 0:
                    counter[i] = counter[i] + 1
                    self._logger.critical("critical - ... Testservice entry {}".format(counter[i]))

    def _handler(self, message):
        self._logger.info("received messages - publishing message")
        self._mqtt_client.publish("/test/service/pub", message)

    def _start(self):
        self._mqtt_client.subscribe("/test/service/sub", self._handler)
        self._stop_thread.clear()
        self._thread.start()

    def _stop(self):
        self._mqtt_client.unsubscribe("/test/service/sub", self._handler)
        self._stop_thread.set()
        self._thread.join()

    def config_information(self):
        return {"config_information":"config_information"}

    def runtime_information(self):
        return {"runtime_information":"runtime_information"}

    @classmethod
    def _get_description(cls):
        return "test service for hippodamia"

    @classmethod
    def _get_schema(cls):
        return {
            "testservice": {
            }
        }


def standalone():
    TestService.standalone()


if __name__ == "__main__":
    TestService.standalone()
