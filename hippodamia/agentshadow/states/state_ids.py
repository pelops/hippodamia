from enum import Enum


class state_ids(Enum):
    ONBOARDING = 1
    ONBOARDED = 2
    ACTIVE = 3
    INACTIVE = 4 
    STOPPED = 5
    ARCHIVECANDIDATE = 6
    ARCHIVED = 7
    PREDEFINED = 8
    LOST = 9


